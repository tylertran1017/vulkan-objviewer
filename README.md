# Overview
This project is an Vulkan based OBJ viewer and exists to explore some fundamentals in rasterization graphics programming using the Vulkan API. Some features of this project include model transformations, Blinn-Phong lighting, texture mapping, procedural texture generation, arcball camera control.

The implementation of this project leverages the [VulkanLaunchpad](https://github.com/cg-tuwien/VulkanLaunchpad) framework from the Tu Wien to abstract some low-level Vulkan implemntation details.

# Compile Instructions
To compile and execute the code:

1. Create a build directory using `mkdir out` 
2. `cd` into the your new directory
3. Once in your new directory, use cmake to generate the Makefile using `cmake ..`
4. Once the Makefile is generated succesfully, compile the code using `make -j 12` if you're cmake generator uses Makefile or the appropriate command for other generators
5. Launch the program from your output directectory using `./ObjViewer {path to obj} {path to texture} {path to ambient occlusion}`

Where `path to obj` is the relative path to the .obj file. 
Where `path to texture` is the relative path to the texture .png file. 
Where `path to ambient occlusion` is the relative path to the ambient occlusion .png file. 

If insufficient arguments are provided, the default "timmy cup" will be rendered.

# Keyboard Controls
The following keyboard inputs are supported

| Input | Functionality                                    |
| ----- | ------------------------------------------------ |
| Q     | Rotate about z-axis CW                           |
| E     | Rotate about z-axis CCW                          |
| A     | Rotate about y-axis CW                           |
| D     | Rotate about y-axis CCW                          |
| W     | Rotate about x-axis CW                           |
| S     | Rotate about x-axis CCW                          |
| X     | Toggle between intrinsic and extrinsic rotations |
| Z     | Scale up                                         |
| Tab   | Toggle ambient occlusion                         |
| F     | Toggle procedural texture                        |
| R     | Increase 'm'                                     |
| V     | Decrease 'm'                                     |

# Reference Images
|   ![](assets/examples/regular_ao.png)    |
| :--------------------------------------: |
|  Regular Textures w/ AO |

|  ![](assets/examples/regular_ao_off.png)  |
| :---------------------------------------: |
| Regular Textures w/o AO |

|  ![](assets/examples/proc_texture_ao_on.png)  |
| :---------------------------------------: |
| Procedural Textures w/ AO |

# Reference Code
A portion of the image loading code is built in reference to [Vulkan Tutorial - Image](https://vulkan-tutorial.com/Texture_mapping/Images). The code which uses some of the implementation from Vulkan Tutorial is highlighted in Image.h and Image.cpp.
