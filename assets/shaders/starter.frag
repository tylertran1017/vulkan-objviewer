#version 450

layout(location = 0) out vec4 color;

layout(location = 0) in vec3 normal;  
layout(location = 1) in vec2 inTex;
layout(location = 2) in vec3 position;

#define PI radians(180)

layout (binding = 0) uniform UBO 
{
	uint m;
	bool proceduralEnable;
	bool aoEnable;
	float ka;
	float kd;
	float ks;
	float gamma;
	vec3 cameraPosition;
	vec3 lightPosition;
	vec3 lightColor;
} ubo;
layout (binding = 1) uniform sampler2D texSampler;
layout (binding = 2) uniform sampler2D aoSampler;
layout (binding = 3) uniform sampler2D noiseSampler;

void main() {
	vec3 cameraDirection = normalize(ubo.cameraPosition - position);
	vec3 lightDirection = normalize(ubo.lightPosition - position);
	vec3 halfway = normalize(cameraDirection + lightDirection); // Blinn-phong because better
	float ambient = ubo.ka;
	float diffuse = ubo.kd * max(dot(normal, lightDirection),0);
	float specular = 0;
	if(diffuse != 0){
		specular = ubo.ks * pow(max(dot(normal, halfway),0),ubo.gamma);
	}
	vec3 intensity = (ambient + diffuse + specular) * ubo.lightColor;
	
	vec4 occlusion;
	vec4 outTex;

	if(ubo.aoEnable){
		occlusion = texture(aoSampler, inTex);
	} else {
		occlusion = vec4(1.0, 1.0, 1.0, 1.0);
	}
	if(ubo.proceduralEnable){
		float proceduralColor = 1.0/2.0 * (1.0 + sin(ubo.m * PI * (inTex.x + inTex.y + texture(noiseSampler, inTex).r)));
		outTex = vec4(proceduralColor);
	} else {
		outTex = texture(texSampler, inTex);
	}
	color = occlusion * outTex * vec4(intensity, 1);
}