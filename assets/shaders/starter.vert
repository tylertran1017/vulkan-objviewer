#version 450

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 inNormal; 
layout(location = 2) in vec2 inTex; 

layout(location=0) out vec3 outNormal;
layout(location=1) out vec2 outTex;
layout(location=2) out vec3 outPosition;

//push constants block
layout( push_constant ) uniform constants
{
	mat4 model;
    mat4 viewproj;
} PushConstants;

void main() {
    outNormal = normalize(mat3(PushConstants.model) * inNormal);
    outTex = inTex;
    outPosition = vec3(PushConstants.model * vec4(position, 1.f));
	
    gl_Position = PushConstants.viewproj * 
    PushConstants.model * 
    vec4(position.x, position.y, position.z, 1.f);
}
