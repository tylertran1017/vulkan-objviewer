#include "Image.h"
#include "stb_image.h"

extern VkDevice vk_device;
extern VkPhysicalDevice vk_physical_device;
extern VkQueue vk_queue;

ImageSampler createImageSampler(const char * texturePath)
{
    VkImage textureImage = createTextureImage(texturePath);
    VkImageView textureImageView = createTextureImageView(textureImage, VK_FORMAT_R8G8B8A8_SRGB);
    VkSampler textureSampler = createTextureSampler();

    ImageSampler imageSampler{
        .textureImageView = textureImageView,
        .textureSampler = textureSampler,
    };
    return imageSampler;
}

ImageSampler createProceduralImageSampler(){
    VkImage textureImage = createProceduralTextureImage();
    VkImageView textureImageView = createTextureImageView(textureImage, VK_FORMAT_R32_SFLOAT);
    VkSampler textureSampler = createTextureSampler();

    ImageSampler imageSampler{
        .textureImageView = textureImageView,
        .textureSampler = textureSampler,
    };
    return imageSampler;
}

VkImage createTextureImage(const char * texturePath){    
    // STEP 1 GENERATE TEXTURE
	int texWidth, texHeight, texChannels;
	VKL_LOG("Loading texture from path: [" << texturePath << "]...");
	stbi_uc * texData = stbi_load(texturePath, &texWidth, &texHeight, &texChannels, STBI_rgb_alpha);
	size_t imageSize = texWidth * texHeight * 4;
    
    // STEP 2 CREATE STAGING BUFFER
    VkBuffer stagingBuffer = vklCreateHostCoherentBufferAndUploadData(
        texData, imageSize,
        VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT
    );

    // STEP 3 CREATE VULKAN IMAGE
    VkImage textureImage = vklCreateDeviceLocalImageWithBackingMemory(
        vk_physical_device, 
        vk_device, 
        static_cast<uint32_t>(texWidth),
        static_cast<uint32_t>(texHeight),
        VK_FORMAT_R8G8B8A8_SRGB,
        VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT);

    // STEP 4 UPDATE IMAGE LAYOUT
    transitionImageLayout(textureImage, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
    copyBufferToImage(stagingBuffer, textureImage, static_cast<uint32_t>(texWidth), static_cast<uint32_t>(texHeight));
    transitionImageLayout(textureImage, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

    // STEP 5 CLEANUP AND RETURN
    stbi_image_free(texData);
    vklDestroyHostCoherentBufferAndItsBackingMemory(stagingBuffer);
    return textureImage;
}

VkImage createProceduralTextureImage(){
    // STEP 1 GENERATE TEXTURE
    VKL_LOG("Generating procedural texture");
    auto grid = generateGrid(PROCEDURAL_TEXTURE_SIZE);
    size_t gridSize = PROCEDURAL_TEXTURE_SIZE * PROCEDURAL_TEXTURE_SIZE * 4;
    // STEP 2 CREATE STAGING BUFFER
    VkBuffer stagingBuffer = vklCreateHostCoherentBufferAndUploadData(
        grid.get(), gridSize,
        VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT
    );

    // STEP 3 CREATE VULKAN IMAGE
    VkImage textureImage = vklCreateDeviceLocalImageWithBackingMemory(
        vk_physical_device, 
        vk_device, 
        PROCEDURAL_TEXTURE_SIZE,
        PROCEDURAL_TEXTURE_SIZE,
        VK_FORMAT_R32_SFLOAT,
        VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT);

    // STEP 4 UPDATE IMAGE LAYOUT
    transitionImageLayout(textureImage, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
    copyBufferToImage(stagingBuffer, textureImage, PROCEDURAL_TEXTURE_SIZE, PROCEDURAL_TEXTURE_SIZE);
    transitionImageLayout(textureImage, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

    // STEP 5 CLEANUP AND RETURN
    vklDestroyHostCoherentBufferAndItsBackingMemory(stagingBuffer);
    return textureImage;
}

VkImageView createTextureImageView(VkImage textureImage, VkFormat format) {
    VkImageView textureImageView{};
    VkImageViewCreateInfo viewInfo{
        .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
        .image = textureImage,
        .viewType = VK_IMAGE_VIEW_TYPE_2D,
        .format = format,
        .subresourceRange{
            .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
            .baseMipLevel = 0,
            .levelCount = 1,
            .baseArrayLayer = 0,
            .layerCount = 1,
        }
    };

    vkCreateImageView(vk_device, &viewInfo, nullptr, &textureImageView);
    return textureImageView;
}

VkSampler createTextureSampler() {
    VkSampler textureSampler{};
    VkSamplerCreateInfo samplerInfo{
        .sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
        .magFilter = VK_FILTER_LINEAR,
        .minFilter = VK_FILTER_LINEAR,
        .mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR,
        .addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT,
        .addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT,
        .addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT,
        .anisotropyEnable = VK_FALSE,
        .compareEnable = VK_FALSE,
        .compareOp = VK_COMPARE_OP_ALWAYS,
        .borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK,
        .unnormalizedCoordinates = VK_FALSE,
    };

    vkCreateSampler(vk_device, &samplerInfo, nullptr, &textureSampler);
    return textureSampler;
}

VkCommandBuffer beginSingleTimeCommands() {
    VkCommandBufferAllocateInfo allocInfo{
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
        .commandPool = vklGetCommandPool(),
        .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
        .commandBufferCount = 1,
    };

    VkCommandBuffer commandBuffer;
    vkAllocateCommandBuffers(vk_device, &allocInfo, &commandBuffer);

    VkCommandBufferBeginInfo beginInfo{
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        .flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
    };

    vkBeginCommandBuffer(commandBuffer, &beginInfo);

    return commandBuffer;
}

void endSingleTimeCommands(VkCommandBuffer commandBuffer) {
    vkEndCommandBuffer(commandBuffer);

    VkSubmitInfo submitInfo{
        .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
        .commandBufferCount = 1,
        .pCommandBuffers = &commandBuffer,
    };

    vkQueueSubmit(vk_queue, 1, &submitInfo, VK_NULL_HANDLE);
    vkQueueWaitIdle(vk_queue);

    vkFreeCommandBuffers(vk_device, vklGetCommandPool(), 1, &commandBuffer);
}

void transitionImageLayout(VkImage image, VkImageLayout oldLayout, VkImageLayout newLayout){
	VkCommandBuffer commandBuffer = beginSingleTimeCommands();
	VkImageMemoryBarrier barrier{
        .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
        .oldLayout = oldLayout,
        .newLayout = newLayout,
        .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .image = image,
        .subresourceRange{
            .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
            .baseMipLevel = 0,
            .levelCount = 1,
            .baseArrayLayer = 0,
            .layerCount = 1,
        }
    };
	
	VkPipelineStageFlags sourceStage;
	VkPipelineStageFlags destinationStage;
	if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) {
		barrier.srcAccessMask = 0;
		barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

		sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
		destinationStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
	} else if (oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
		barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

		sourceStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
		destinationStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
	}
	vkCmdPipelineBarrier(commandBuffer, sourceStage, destinationStage, 0, 0, nullptr, 0, nullptr, 1, &barrier);
	endSingleTimeCommands(commandBuffer);
}

void copyBufferToImage(VkBuffer buffer, VkImage image, uint32_t width, uint32_t height) {
    VkCommandBuffer commandBuffer = beginSingleTimeCommands();
	VkBufferImageCopy region{
      	.bufferOffset = 0,
        .bufferRowLength = 0,
        .bufferImageHeight = 0,
        .imageSubresource{
            .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
            .mipLevel = 0,
            .baseArrayLayer = 0,
            .layerCount = 1,
        },
        .imageOffset = {0, 0, 0},
        .imageExtent = {width, height, 1},  
    };
	vkCmdCopyBufferToImage(commandBuffer, buffer, image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);
    endSingleTimeCommands(commandBuffer);
}