// Image related utility functions
#include <vulkan/vulkan.h>
#include <vulkan/vulkan.hpp>
#include <VulkanLaunchpad.h>
#include <string>
#include <vector>
#include <memory>
#include "Noise.h"

#define PROCEDURAL_TEXTURE_SIZE 1024

struct ImageSampler
{
    VkImageView textureImageView{};
    VkSampler textureSampler{};
};

ImageSampler createImageSampler(const char * texturePath);
ImageSampler createProceduralImageSampler();

VkImage createTextureImage(const char * texturePath);
VkImage createProceduralTextureImage();
VkImageView createTextureImageView(VkImage textureImage, VkFormat format);
VkSampler createTextureSampler();
VkCommandBuffer beginSingleTimeCommands();
void endSingleTimeCommands(VkCommandBuffer commandBuffer);
void transitionImageLayout(VkImage image, VkImageLayout oldLayout, VkImageLayout newLayout);
void copyBufferToImage(VkBuffer buffer, VkImage image, uint32_t width, uint32_t height);
