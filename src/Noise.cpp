#include "Noise.h"

std::vector<std::vector<float>> randoms(RANDOM_SIZE + 1, std::vector<float>(RANDOM_SIZE + 1));

void initRandoms(){
    std::mt19937 rng(SEED);
    std::uniform_real_distribution<float> distribution(0.f, 1.f);
    for(auto &row : randoms){
        for(auto &cell : row){
            cell = distribution(rng);
        }
    }
};

float noise(float x, float y){
    auto x_floor = static_cast<int>(x);
    auto y_floor = static_cast<int>(y);

    auto x_frac = x - x_floor;
    auto y_frac = y - y_floor;

    // smoothstep of fractional component
    auto u = ((6 * x_frac - 15) * x_frac + 10) * x_frac  * x_frac  * x_frac;
    auto v = ((6 * y_frac - 15) * y_frac + 10) * y_frac  * y_frac  * y_frac;
    
    // get vertices in cell (x to x+1) and (y to y+1)
    auto v00 = randoms[x_floor][y_floor];           // top left
    auto v01 = randoms[x_floor + 1][y_floor];       // top right
    auto v10 = randoms[x_floor][y_floor + 1];       // bottom left
    auto v11 = randoms[x_floor + 1][y_floor + 1];   // bottom right

    // linearly interpolate - horizontal then vertical
    return std::lerp(std::lerp(v00,v01,u), std::lerp(v10,v11,u), v);
}

float turbulence(float x, float y, int octave){
    auto returnValue = 0.f;
    auto totalAmplitude = 0.f;
    
    for(auto i = 0; i < octave; i++){
        auto amplitude = static_cast<float>(std::pow(0.5f, i));
        auto frequency = static_cast<float>(std::pow(2.0f, i));

        returnValue += amplitude * noise(frequency * x, frequency * y);
        totalAmplitude += amplitude;
    }

    // return normalized value
    return returnValue / totalAmplitude;
}

std::unique_ptr<float[]> generateGrid(int size){
    initRandoms();
    auto grid = std::make_unique<float[]>(size * size);
    for(auto y = 0; y < size; y++){
        for(auto x = 0; x < size; x++){
            auto nx = static_cast<float>(x) / size * BASE_NUM_RANDOM_VERTICES;
            auto ny = static_cast<float>(y) / size * BASE_NUM_RANDOM_VERTICES;
            grid[y * size + x] = turbulence(nx, ny, OCTAVE_LEVEL);
        }
    }
    return grid;
}

