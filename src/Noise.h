#include <iostream>
#include <random>
#include <cmath>
#include <memory>

#define SEED 57
#define BASE_NUM_RANDOM_VERTICES 16
#define OCTAVE_LEVEL 5
#define RANDOM_SIZE (BASE_NUM_RANDOM_VERTICES * (1 << (OCTAVE_LEVEL-1)))

// initializes random grid
void initRandoms(int size, uint32_t seed);

// return the noise for a given floating point coordinate
float noise(float x, float y);

// return the turbulence at an integer coordinate at octave level
float turbulence(float x, float y, int octave);

// returns a heap allocated turbulence grid
std::unique_ptr<float[]> generateGrid(int size);

