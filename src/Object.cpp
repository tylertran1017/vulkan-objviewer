#define STB_IMAGE_IMPLEMENTATION
#include "Object.h"
#include "Path.hpp"
#include "Image.h"
#include "stb_image.h"

// buffers that will live on the GPU.
// No geometry retained on the CPU, all data sent to the GPU.

extern std::shared_ptr<shared::Path> path;

uint32_t mNumObjectIndices;
VkBuffer mObjectVertexData;
VkBuffer mObjectIndices;
VkBuffer mLighting;
ImageSampler textureImageSampler;
ImageSampler ambientOcclusionImageSampler;
ImageSampler noiseImageSampler;

// A pipeline that can be used for HW3
VkPipeline pipeline;
VkDescriptorSet ds;

// MVP matrices that are updated interactively
ObjectPushConstants pushConstants;
LightingUBO lightingUBO;

TransformParameters tfp;
glm::mat4 viewproj;

void createTextureImageSampler(std::string texturePath){
	textureImageSampler = createImageSampler(texturePath.c_str());
}

void createAmbientOcclusionImageSampler(std::string aoPath){
	ambientOcclusionImageSampler = createImageSampler(aoPath.c_str());
}

void createNoiseImageSampler(){
	noiseImageSampler = createProceduralImageSampler();
}

// Organize geometry data and send it to the GPU 
void objectCreateGeometryAndBuffers(std::string objectPath) 
{
	VklGeometryData data = vklLoadModelGeometry(objectPath);
	
	// Indices
	auto indices = data.indices;

	// Generate vertex data and do some preprocessing
	auto vertexData = generateVertexData(data);

	mNumObjectIndices = static_cast<uint32_t>(indices.size());
	const auto device = vklGetDevice();
	auto dispatchLoader = vk::DispatchLoaderStatic();

	// 1. Vertex BUFFER (Buffer, Memory, Bind 'em together, copy data into it)
	{ 
		// Use VulkanLaunchpad functionality to manage buffers
		// All vertex data is in one vector, copied to one buffer
		// on the GPU
		mObjectVertexData = vklCreateHostCoherentBufferAndUploadData(
			vertexData.data(), sizeof(vertexData[0]) * vertexData.size(),
			VK_BUFFER_USAGE_VERTEX_BUFFER_BIT);
	}

	// 2. INDICES BUFFER (Buffer, Memory, Bind 'em together, copy data into it)
	{
		mObjectIndices = vklCreateHostCoherentBufferAndUploadData(
			indices.data(), sizeof(indices[0]) * indices.size(),
			VK_BUFFER_USAGE_INDEX_BUFFER_BIT);
	}

	// 3. LIGHTING UNIFORM BUFFER (Buffer, Memory, Bind 'em together, copy data into it)
	{
		mLighting = vklCreateHostCoherentBufferAndUploadData(
		&lightingUBO, sizeof(LightingUBO),
		VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT);
	}

	// Now Create the pipeline
	objectCreatePipeline();
}

std::vector<Vertex> generateVertexData(VklGeometryData & data){
	auto positions = data.positions;
	auto normals = data.normals;
	auto textureCoordinates = data.textureCoordinates;

	// Find min and max values
	float min_x{positions.front().x}, max_x{positions.front().x};
	float min_y{positions.front().y}, max_y{positions.front().y};
	float min_z{positions.front().z}, max_z{positions.front().z};
	for (const auto & position: positions)
	{
		min_x = std::min(min_x, position.x);
		min_y = std::min(min_y, position.y);
		min_z = std::min(min_z, position.z);
		max_x = std::max(max_x, position.x);
		max_y = std::max(max_y, position.y);
		max_z = std::max(max_z, position.z);
	}
	glm::vec3 centroid{(max_x+min_x)/2.f, (max_y+min_y)/2.f, (max_z+min_z)/2.f};

	for(auto & position: positions)
	{
		position -= centroid;
		position = 2.f/(max_x-min_x) * position;
	}

	// Create a vector to interleave and pack all vertex data into one vector.
	std::vector<Vertex> vertexData( positions.size() );
	for( unsigned int i = 0; i < vertexData.size(); i++ ) {
		vertexData[i].position = positions[i];
		vertexData[i].normal = normals[i];
		vertexData[i].textureCoordinates = textureCoordinates[i];
	}
	return vertexData;
}

void updateModelMatrix(glm::mat4 & M, TransformParameters & param)
{
	M =
	// Extrinsic Rotation
	glm::rotate( glm::mat4(1.f), param.e_rotx, glm::vec3(1.f, 0.f, 0.f)) * 
	glm::rotate( glm::mat4(1.f), param.e_roty, glm::vec3(0.f, 1.f, 0.f)) *
	glm::rotate( glm::mat4(1.f), param.e_rotz, glm::vec3(0.f, 0.f, 1.f)) *
	// Scaling
	M *
	// Intrinsic Rotation
	glm::rotate( glm::mat4(1.f), param.i_rotx, glm::vec3(1.f, 0.f, 0.f)) * 
	glm::rotate( glm::mat4(1.f), param.i_roty, glm::vec3(0.f, 1.f, 0.f)) *
	glm::rotate( glm::mat4(1.f), param.i_rotz, glm::vec3(0.f, 0.f, 1.f)) *
	glm::scale( glm::mat4(1.f), glm::vec3(param.scale,param.scale,param.scale));
}

// Cleanup buffers and pipeline created on the GPU 
void objectDestroyBuffers() {
	auto device = vklGetDevice();
	vkDeviceWaitIdle( device );
	vklDestroyGraphicsPipeline(pipeline);
	vklDestroyHostCoherentBufferAndItsBackingMemory( mObjectVertexData );
	vklDestroyHostCoherentBufferAndItsBackingMemory( mObjectIndices );
	vklDestroyHostCoherentBufferAndItsBackingMemory( mLighting );
}

void objectDraw() {
	objectDraw( pipeline );
}

void objectDraw(VkPipeline pipeline)
{
	if (!vklFrameworkInitialized()) {
		VKL_EXIT_WITH_ERROR("Framework not initialized. Ensure to invoke vklFrameworkInitialized beforehand!");
	}
	const vk::CommandBuffer& cb = vklGetCurrentCommandBuffer();
	auto currentSwapChainImageIndex = vklGetCurrentSwapChainImageIndex();
	assert(currentSwapChainImageIndex < vklGetNumFramebuffers());
	assert(currentSwapChainImageIndex < vklGetNumClearValues());

	cb.bindPipeline(vk::PipelineBindPoint::eGraphics, pipeline);

	cb.bindVertexBuffers(0u, { vk::Buffer{ objectGetVertexBuffer() } }, { vk::DeviceSize{ 0 } });
	cb.bindIndexBuffer(vk::Buffer{ objectGetIndicesBuffer() }, vk::DeviceSize{ 0 }, vk::IndexType::eUint32);
	vklBindDescriptorSetToPipeline(ds, pipeline);

	// update push constants on every draw call and send them over to the GPU.
    // upload the matrix to the GPU via push constants
	objectUpdateConstants();

	// lighting uniform buffer
	vklCopyDataIntoHostCoherentBuffer(mLighting, &lightingUBO, sizeof(lightingUBO));

    vklSetPushConstants(
			pipeline, 
			VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT, 
			&pushConstants, 
			sizeof(ObjectPushConstants)
		);

	cb.drawIndexed(objectGetNumIndices(), 1u, 0u, 0, 0u);
}

VkBuffer objectGetVertexBuffer() {
	return static_cast<VkBuffer>(mObjectVertexData);
}

VkBuffer objectGetIndicesBuffer() {
	return static_cast<VkBuffer>(mObjectIndices);
}

uint32_t objectGetNumIndices() {
	return mNumObjectIndices;
}

void objectCreatePipeline() {
	// ------------------------------
	// Initialize push constants
	// ------------------------------
	// No need to initialize push constants as they will be updated in objectUpdateConstants

	// ------------------------------
	// Pipeline creation
	// ------------------------------

	auto const vertShaderPath = path->Get("shaders/starter.vert");
	auto const fragShaderPath = path->Get("shaders/starter.frag");
	
	VklGraphicsPipelineConfig config{};
		config.enableAlphaBlending = false;
		// path to shaders may need to be modified depending on the location
		// of the executable
		config.vertexShaderPath = vertShaderPath.c_str();
		config.fragmentShaderPath = fragShaderPath.c_str();
		
		// Can set polygonDrawMode to VK_POLYGON_MODE_LINE for wireframe rendering
		config.polygonDrawMode = VK_POLYGON_MODE_FILL;
		config.triangleCullingMode = VK_CULL_MODE_BACK_BIT;

		// Binding for vertex buffer, using 1 buffer with per-vertex rate.
		// This will send per-vertex data to the GPU.
		config.vertexInputBuffers.emplace_back(VkVertexInputBindingDescription{
			.binding = 0,
			.stride = sizeof(Vertex),
			.inputRate = VK_VERTEX_INPUT_RATE_VERTEX,
		});

		// Positions at location 0
		config.inputAttributeDescriptions.emplace_back(VkVertexInputAttributeDescription{
			//.location = static_cast<uint32_t>(config.inputAttributeDescriptions.size()),
			.location = 0,
			.binding = 0,
			.format = VK_FORMAT_R32G32B32_SFLOAT,
			.offset = offsetof(Vertex, position),
		});

		// Normals at location 1
		config.inputAttributeDescriptions.emplace_back(VkVertexInputAttributeDescription{
			//.location = static_cast<uint32_t>(config.inputAttributeDescriptions.size()),
			.location = 1,
			.binding = 0,
			.format = VK_FORMAT_R32G32B32_SFLOAT,
			.offset = offsetof(Vertex, normal),
		});

		// Texture coordinates at location 2
		config.inputAttributeDescriptions.emplace_back(VkVertexInputAttributeDescription{
			//.location = static_cast<uint32_t>(config.inputAttributeDescriptions.size()),
			.location = 2,
			.binding = 0,
			.format = VK_FORMAT_R32G32B32_SFLOAT,
			.offset = offsetof(Vertex, textureCoordinates),
		});
            
		// Light uniform buffer
		config.descriptorLayout.emplace_back( VkDescriptorSetLayoutBinding{
			.binding = 0,
			.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
			.descriptorCount = 1,
			.stageFlags = VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT,
		});
		// Combined image sampler - texture
		config.descriptorLayout.emplace_back( VkDescriptorSetLayoutBinding{
			.binding = 1,
			.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
			.descriptorCount = 1,
			.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
		});
		// Combined image sampler - ambient occlusion
		config.descriptorLayout.emplace_back( VkDescriptorSetLayoutBinding{
			.binding = 2,
			.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
			.descriptorCount = 1,
			.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
		});
		// Combined image sampler - procedural noise
		config.descriptorLayout.emplace_back( VkDescriptorSetLayoutBinding{
			.binding = 3,
			.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
			.descriptorCount = 1,
			.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
		});

		// Push constants should be available in both the vertex and fragment shaders
		config.pushConstantRanges.emplace_back(VkPushConstantRange{
			.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT | VK_SHADER_STAGE_VERTEX_BIT,
			.offset = 0,
			.size = sizeof(ObjectPushConstants),
		});
		
	pipeline = vklCreateGraphicsPipeline( config );

	// Setting up uniform buffers
	std::array<VkDescriptorPoolSize,2> dps = {
		VkDescriptorPoolSize{
			.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
			.descriptorCount = 1,
		},
		VkDescriptorPoolSize{
			.type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
			.descriptorCount = 3,
		}
	};

	VkDescriptorPoolCreateInfo dpci = {
		.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
		.maxSets = 1,
		.poolSizeCount = static_cast<uint32_t>(dps.size()),
		.pPoolSizes = dps.data(),
	};

	VkDescriptorPool dp;
	vkCreateDescriptorPool(vklGetDevice(), &dpci, nullptr, &dp);

	auto const dsl = vklGetDescriptorLayout(pipeline);
	VkDescriptorSetAllocateInfo dsai = {
		.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
		.descriptorPool = dp,
		.descriptorSetCount = 1,
		.pSetLayouts = &dsl,
	};
	vkAllocateDescriptorSets(vklGetDevice(), &dsai, &ds);
	
	// Uniform buffer
	VkDescriptorBufferInfo dbi = {
		.buffer = mLighting,
		.offset = 0,
		.range = sizeof(LightingUBO)
	};
	// Texture
	VkDescriptorImageInfo dii_text = {
		.sampler = textureImageSampler.textureSampler,
		.imageView = textureImageSampler.textureImageView,
		.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
	};
	// Ambient occlusion
	VkDescriptorImageInfo dii_ao = {
		.sampler = ambientOcclusionImageSampler.textureSampler,
		.imageView = ambientOcclusionImageSampler.textureImageView,
		.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
	};
	// Procedural noise
	VkDescriptorImageInfo dii_pn = {
		.sampler = noiseImageSampler.textureSampler,
		.imageView = noiseImageSampler.textureImageView,
		.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
	};

	std::array<VkWriteDescriptorSet,4> wds = {
		VkWriteDescriptorSet{
			.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
			.dstSet = ds,
			.dstBinding = 0,
			.dstArrayElement = 0,
			.descriptorCount = 1,
			.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
			.pBufferInfo = &dbi,	
		},
		VkWriteDescriptorSet{
			.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
			.dstSet = ds,
			.dstBinding = 1,
			.dstArrayElement = 0,
			.descriptorCount = 1,
			.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
			.pImageInfo  = &dii_text,
		},
		VkWriteDescriptorSet{
			.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
			.dstSet = ds,
			.dstBinding = 2,
			.dstArrayElement = 0,
			.descriptorCount = 1,
			.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
			.pImageInfo  = &dii_ao,
		},
		VkWriteDescriptorSet{
			.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
			.dstSet = ds,
			.dstBinding = 3,
			.dstArrayElement = 0,
			.descriptorCount = 1,
			.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
			.pImageInfo  = &dii_pn,
		},
	};

	vkUpdateDescriptorSets(vklGetDevice(), static_cast<uint32_t>(wds.size()), wds.data(), 0, nullptr);
}

// Function to update push constants.
// For the starter example, only the model matrix is updated.
void objectUpdateConstants() {
	updateModelMatrix(pushConstants.model, tfp);
	pushConstants.viewproj = viewproj;
}
