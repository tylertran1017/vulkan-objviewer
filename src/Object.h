#include <vulkan/vulkan.h>
#include <vulkan/vulkan.hpp>
#include <VulkanLaunchpad.h>
#include <string>
#include <vector>
#include <random>

#define ANGULAR_VELOCITY 0.05f
#define SCALE_VELOCITY 0.1f

// Struct to pack object vertex data
struct Vertex {
	glm::vec3 position;
    glm::vec3 normal;
	glm::vec2 textureCoordinates;
}; 

// Send model, view and projection matrices as push constants
// which are packed in this struct
struct ObjectPushConstants {
    glm::mat4 model = glm::mat4{1.f};
    glm::mat4 viewproj;
};

struct LightingUBO{
	alignas(4) uint32_t m = 24;
	alignas(4) bool proceduralEnable = false;
	alignas(4) bool aoEnable = true;
	alignas(4) float ka = 0.05f;
	alignas(4) float kd = 0.7f;
	alignas(4) float ks = 0.5f;
	alignas(4) float gamma = 16.f;
	alignas(16) glm::vec3 cameraPosition = {};
	alignas(16) glm::vec3 lightPosition = glm::vec3{2.f,-2.f,2.f};
	alignas(16) glm::vec3 lightColor = glm::vec3{1.f,1.f,1.f};
};

struct TransformParameters {
	float scale = 1;
	float i_rotx;
	float i_roty;
	float i_rotz;
	float e_rotx;
	float e_roty;
	float e_rotz;
};

void createTextureImageSampler(std::string texturePath);
void createAmbientOcclusionImageSampler(std::string  aoPath);
void createNoiseImageSampler();

std::vector<Vertex> generateVertexData(VklGeometryData & data);
void updateModelMatrix(glm::mat4 & M, TransformParameters & param);

void objectCreateGeometryAndBuffers(std::string objectPath);
void objectDestroyBuffers();
void objectDraw();

void objectDraw(VkPipeline pipeline);

VkBuffer objectGetVertexBuffer();
VkBuffer objectGetIndicesBuffer();
uint32_t objectGetNumIndices();

void objectCreatePipeline();
void objectUpdateConstants();